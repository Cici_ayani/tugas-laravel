<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\casts\Attribute;


class categories extends Model
{
    use HasFactory;
    /**
     * fillable
     * 
     * @var array
     */
    protected $fillable = [
        'image', 'name', 'description'
    ];
    /**
     * products
     * 
     * @return void
     */
    public function products()
    {
        return $this->hasMany(products::class);
    }

    /**
     * image
     * @return attribute
     */
    public function image(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => asset ('/storage/categories/' .$value),
        );
    }
}
<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;

class transactions extends Model
{
    use HasFactory;
    /**
     * fillable
     * 
     * @var array
     */
    protected $fillable = [
        'cashier_id', 'customer_id', 'invoice', 'cash', 'change', 'discount', 'grand_total', 'table_total'
    ];

    /**
     * details
     * 
     * @return void
     */
    public function details()
    {
        return $this->hasMany(transaction_details::class);
    }

    /**
     * customers
     * 
     * @return void
     */
    public function costumer()
    {
        return $this->belongsTo(customers::class);
    }

    /**
     * user
     * 
     * @return void
     */
    public function User()
    {
        return $this->belongsTo(user::class, 'cashier_id');
    }

    /**
     * profit
     * 
     * @return void
     */
    public function profit()
    {
        return $this->belongsTo(profits::class);
    }
    /**
     * createdAt
     * 
     * @return attribute
     */
    protected function createdAt(): attribute
    {
        return Attribute::make(
            get: fn ($value) => Carbon:: perse($value)->format('d-M-Y H:i:s'),
        );
    }
}
<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;

class profits extends Model
{
    use HasFactory;
    /**
     * fillable
     * 
     * @var array
     */
    protected $fillable = [
        'transaction_id', 'total'
    ];

    /**
     * transavtions
     * 
     * @return void
     */
    public function transactions()
    {
        return $this->belongsTo(transactions::class);
    }

    /**
     * createdAt
     * 
     * @return attribute
     */
    protected function createdAt(): attribute
    {
        return Attribute::make(
            get: fn ($value) => Carbon:: parse($value)->format('d-M-Y H:i:s'),
        );
    }
}
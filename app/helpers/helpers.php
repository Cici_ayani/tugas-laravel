<?php
if (function_exists('formatprice')) {
    /**
     * formatprice
     * 
     * @porom mixed $str
     * @return void
     */
    function formatprice($str)
    {
        return 'Rp. ' . number_format($str, '0', '', '.');
    }
}
<?php

namespace Database\Seeders;

use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //create user
        $user = User::create([
            'name' => 'Administrator',
            'email' => 'admin@gmail.com',
            'password' => brcrypt('password'),
        ]);

        //get all permissions
        $permissions = permissions::all()

        //get role admin
        $role =Role::find(1);

        //assign permission to role
        $role->syncPermissions($permissions);

        //assign rool to user
        $user->assignRole($role);
    }
}
